const http = require("http");
const hostname = '127.0.0.1'
const port = process.env.PORT || 3003;
const express = require("express");
const webserver = express(),
healthCheckRes = {
  health: "OK"
};
compression = require('compression');

webserver.use([compression(), express.static("public")]);

const server = http.createServer(webserver)

//Simple get API call
webserver.get(/\/healthcheck/i, (req, res, next) => {
  res.send(healthCheckRes);
});

//Simple get API call
webserver.get(/\/project\/1/i, (req, res, next) => {
  res.send("This is the NPM project");
});

//End point render html file 
// webserver.get(/^\//, (req, res) => {
//   res.sendFile(`${__dirname}/index.html`);
// });

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`)
})
